using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test01 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CVector2 a = new CVector2(16.2f,5.36f);
        CVector2 b = new CVector2(5f, 9.64f);
        Debug.Log("Suma: " + (a + b));

        CVector2 c = new CVector2(200f, 56.3f);
        CVector2 d = new CVector2(25.45f, 6f);
        Debug.Log("Resta " + (c - d));

        CVector3 e = new CVector3(189f, 2.3f, 64.2f);
        CVector3 f = new CVector3(1f, 3f, 0.5f);
        Debug.Log("Multiplicación " + (e * f));

        CVector3 g = new CVector3(5f, 6.41f, 15f);
        Debug.Log("Multiplación escalar " + (g * 5));

        CVector2 h = new CVector2(5f, 2f);
        CVector2 i = new CVector2(5f,2f);
        Debug.Log("Igual " + (h == i));

        CVector3 j = new CVector3(8f, 6f, 4.5f);
        CVector3 k = new CVector3(6.4f, 2.5f, 3f);
        Debug.Log ("Diferente" + (k == j));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
