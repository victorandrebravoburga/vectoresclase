using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Unity.Burst.Intrinsics;
using Unity.Mathematics;
using UnityEngine;

public class CVector2
{

    public float x {get; set;}
    public float y {get; set;}

public CVector2()
{
    x = 0f;
    y = 0f;
}

public CVector2(float px, float py)
{
    this.x = px;
    this.y = py;
}

//suma

public static CVector2 operator +(CVector2 a, CVector2 b)
{
    return new CVector2(a.x + b.x, a.y + b.y);
}

//resta

public static CVector2 operator -(CVector2 a, CVector2 b)
{
    return new CVector2(a.x - b.x, a.y - b.y);
}

//multiplicación

public static CVector2 operator *(CVector2 a, CVector2 b)
{
    return new CVector2(a.x * b.x, a.y * b.y);
}

//multipliación escalar

public static CVector2 operator *(CVector2 a, float s)
{
    return new CVector2(a.x * s, a.y * s);
}

//igualdad

public static bool operator ==(CVector2 a, CVector2 b)
{
    const float epsilon = 0.001f;
    if(Mathf.Abs(a.x - b.x) <= epsilon && Mathf.Abs(a.y - b.y) <= epsilon)
        return true;
    else
        return false;
}

//desigualdad

public static bool operator !=(CVector2 a, CVector2 b)
{
    return !(a == b);
}

public override string ToString()
{
    return string.Format("{0}, {1}", x, y);
}

}
