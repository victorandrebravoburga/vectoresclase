using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Unity.Burst.Intrinsics;
using UnityEngine;

public class CVector3
{

    public float x {get; set;}
    public float y {get; set;}
    public float z {get; set;}

public CVector3()
{
    x = 0f;
    y = 0f;
    z = 0f; 
}

public CVector3(float px, float py, float pz)
{
    this.x = px;
    this.y = py;
    this.z = pz;
}

//suma

public static CVector3 operator +(CVector3 a, CVector3 b)
{
    return new CVector3(a.x + b.x, a.y + b.y, a.z + b.z);
}

//resta

public static CVector3 operator -(CVector3 a, CVector3 b)
{
    return new CVector3(a.x - b.x, a.y - b.y, a.z - b.z);
}

//multiplicación

public static CVector3 operator *(CVector3 a, CVector3 b)
{
    return new CVector3(a.x * b.x, a.y * b.y, a.z * b.z);
}

//multipliación escalar

public static CVector3 operator *(CVector3 a, float s)
{
    return new CVector3(a.x * s, a.y * s, a.z * s);
}

//igualdad

public static bool operator ==(CVector3 a, CVector3 b)
{
    const float epsilon = 0.001f;
    if(Mathf.Abs(a.x - b.x) <= epsilon && Mathf.Abs(a.y - b.y) <= epsilon && Mathf.Abs(a.z - b.z) <= epsilon)
        return true;
    else
        return false;
}

//desigualdad

public static bool operator !=(CVector3 a, CVector3 b)
{
    return !(a == b);
}

public override string ToString()
{
    return string.Format("{0}, {1}, {2}", x, y, z);
}

}
